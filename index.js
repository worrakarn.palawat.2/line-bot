const https = require("https")
const express = require("express")
const axios = require("axios")
const app = express()
const PORT = process.env.PORT || 3000
const TOKEN = process.env.LINE_ACCESS_TOKEN

app.use(express.json())
app.use(express.urlencoded({
  extended: true
}))

app.get("/", (req, res) => {
  res.sendStatus(200)
})

app.post("/webhook", function(req, res) {
  res.send("HTTP POST request sent to the webhook URL!")
  // If the user sends a message to your bot, send a reply message
  console.log(req.body)
  if (req.body.events[0].type === "message") {
    
    // Request header
    const headers = {
      "Content-Type": "application/json",
      "Authorization": "Bearer " + TOKEN
    }

    // Message data, must be stringified
    const dataString = JSON.stringify({
      replyToken: req.body.events[0].replyToken,
      messages: [
        {
          "type": "text",
          "text": "Hello, user"
        },
        {
          "type": "text",
          "text": "May I help you?"
        }
      ]
    })

    const lineOptions = {
      method: 'post',
      url: 'https://api.line.me/v2/bot/message/reply',
      headers: headers,
      data: dataString
    }
    
    axios(lineOptions)
      .then(function (response) {
        // console.log(JSON.stringify(response.data))
      })
      .catch(function (error) {
        console.log(error)
      })

  }
})

app.get("/weather", function(req, res) {
  const weatherOptions = {
    method: 'get',
    url: 'https://api.openweathermap.org/data/2.5/weather?units=metric&type=accurate&zip=76000,th&appid=5ccaf23eb4d91b9ed947515a5531012f',
  }

  axios(weatherOptions)
    .then(function (response) {
      const data = response.data
      // Request header
      const headers = {
        "Content-Type": "application/json",
        "Authorization": "Bearer " + TOKEN
      }

      // Message data, must be stringified
      const message = `City: ${data.name}\nWeather: ${data.weather[0].description}\nTemperature: ${data.main.temp}`

      const dataString = JSON.stringify({
        to: 'U9dd46402e445a9b791e166e35b56b1c7',
        messages: [
          {
            "type": "text",
            "text": message
          }
        ]
      })

      const pushOptions = {
        method: 'post',
        url: 'https://api.line.me/v2/bot/message/push',
        headers: headers,
        data: dataString
      }

      axios(pushOptions)
        .then(function (response) {
          // console.log(JSON.stringify(response.data))
          res.sendStatus(200)
        })
        .catch(function (error) {
          console.log(error)
        })

    })
    .catch(function (error) {
      console.log(error)
    })
})

app.listen(PORT, () => {
  console.log(`Example app listening at http://localhost:${PORT}`)
})